#### Установка

+ ставим зависисмости
```bash
    easy_install -U pip
    pip install -U pip virtualenv fabric jinja2
```

+ клонируем проект с битбакета и ставим локально в debian/ubuntu
```bash
    mkdir -p quantify/src
    cd quantify/src
    git clone git@bitbucket.org:skomoroh/quantify.git .
    ./setup.sh
```







