#!/bin/bash
cd "$(dirname "$0")"
mkdir -p static media db log locale
chmod a+Xwr media db log
virtualenv venv
cp app/app/local_settings_default.py app/app/local_settings.py
source venv/bin/activate && pip install -U -r requirements.txt
source venv/bin/activate && app/manage.py migrate
source venv/bin/activate && app/manage.py createsuperuser --username=root --email='zzz@zzz.zz' --noinput
source venv/bin/activate && app/manage.py changepassword root
source venv/bin/activate && find app -name 'data_*.json' -exec app/manage.py loaddata {} \;

