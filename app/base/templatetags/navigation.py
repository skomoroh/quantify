# -*- coding: utf-8 -*-
import re

from django import template
from django.core.urlresolvers import reverse, NoReverseMatch

register = template.Library()

@register.simple_tag(takes_context=True)
def nav_active(context, pattern_or_urlname):
    try:
        pattern = reverse(pattern_or_urlname)
    except NoReverseMatch:
        pattern = pattern_or_urlname

    if 'request' in context:
        if pattern in context['request'].path:
            return 'active'

    return '' 
