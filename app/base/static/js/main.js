$(function() {
    $('.faq-title').on('click', function() {
        title = $(this).html();
        text = $(this).parent().children('.faq-desc').html();
        modal = $('#Modal');
        modal.find('.modal-header>h4').html(title);
        modal.find('.modal-body>p').html(text);
        modal.modal('show');
        return false;
    });
});
