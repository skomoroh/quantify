# -*- coding: utf-8 -*-

import os
import re
# from django.utils.translation import ugettext as _
from django.utils.translation import ugettext_lazy as _


try:
    from local_settings import *
except ImportError:
    pass


BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


ALLOWED_HOSTS = ['*',]

INTERNAL_IPS = ['127.0.0.1', 'localhost']

SECRET_KEY = '8-gl%1s&ugebz#u&^92m1+05og!+ux5=^fr9&-7*4oj=#^4$e6'

INSTALLED_APPS = [
    'app',
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.contenttypes',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'crispy_forms',
    'modeltranslation',
    'sorl.thumbnail',
    'django_mandrill',
    'ckeditor',
    'ckeditor_uploader',
    'base',
    'users',
    'pages',
    'registration',
    'social.apps.django_app.default',
    'django.contrib.admin',
    'django.contrib.messages',
    'talk',
    'quiz',
    'report',
    'sample',
    'contact',
]

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
     # CacheMiddleware
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

if DEBUG:
    INSTALLED_APPS += (
        'debug_toolbar.apps.DebugToolbarConfig',
    )
    MIDDLEWARE_CLASSES += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    )


ROOT_URLCONF = 'app.urls'
WSGI_APPLICATION = 'app.wsgi.application'


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        # 'APP_DIRS': True,
        'OPTIONS': {
            'debug': DEBUG,
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'social.apps.django_app.context_processors.backends',
                'social.apps.django_app.context_processors.login_redirect',
                'talk.context_processors.talk',
            ],
            'loaders': [
                ('django.template.loaders.cached.Loader', [
                    'django.template.loaders.filesystem.Loader',
                    'django.template.loaders.app_directories.Loader',
                ]),
            ],
        },
    },
]

if DEBUG:
    del TEMPLATES[0]['OPTIONS']['loaders']
    TEMPLATES[0]['APP_DIRS'] = True


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db', 'db.sqlite3'),
    },
}
SQLAL_DB = 'sqlite:///%s' % os.path.join(BASE_DIR, 'db', 'db.sqlite3')
# SQLAL_DB = 'postgresql://django:django@127.0.0.1:5432/django'




LANGUAGES = [
    ('ru', _('Russian')),
    ('en', _('English')),
    # ('es', _('Spanish')),
    # ('fr', _('French')),
    # ('ja', _('Japanese')),
]
LANGUAGE_CODE = 'ru-RU'
TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = True
USE_TZ = True

MODELTRANSLATION_DEFAULT_LANGUAGE = 'ru'
MODELTRANSLATION_DEBUG = DEBUG

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

REGISTRATION_OPEN = True
ACCOUNT_ACTIVATION_DAYS = 7
REGISTRATION_AUTO_LOGIN = True
LOGIN_REDIRECT_URL = 'home'
LOGIN_URL = '/accounts/login/'

# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_BACKEND = 'django_mandrill.mail.backends.mandrillbackend.EmailBackend'
MANDRILL_API_KEY = '-6GYeyaQJOrpIU-_A4Xyrg'
DEFAULT_FROM_EMAIL = 'robot@0v9.ru'
DEFAULT_TO_EMAIL = 'robot@0v9.ru'
SERVER_EMAIL = 'robot@0v9.ru'

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'
CKEDITOR_UPLOAD_PATH = 'uploads'
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'full',
        'entities': False,
    },
}
CKEDITOR_IMAGE_BACKEND = 'pillow'

AUTHENTICATION_BACKENDS = (
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.vk.VKOAuth2',
    'social.backends.google.GoogleOAuth2',
    'django.contrib.auth.backends.ModelBackend',
)

SOCIAL_AUTH_URL_NAMESPACE = 'social'
SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL = True
SOCIAL_AUTH_SLUGIFY_USERNAMES = True
SOCIAL_AUTH_LOGIN_REDIRECT_URL = 'home'
# SOCIAL_AUTH_LOGIN_URL = '/'
SOCIAL_AUTH_PROTECTED_USER_FIELDS = ['email', 'first_name', 'last_name', ]

# https://developers.facebook.com/apps/
SOCIAL_AUTH_FACEBOOK_KEY = '1567873506858220'
SOCIAL_AUTH_FACEBOOK_SECRET = 'e7970f4d1e0ff5ee6c3b70390d4231d9'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email',]
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {'locale': 'ru_RU'}

# https://vk.com/dev
SOCIAL_AUTH_VK_OAUTH2_KEY = '5358005'
SOCIAL_AUTH_VK_OAUTH2_SECRET = 'B13gVMTgxBNuLV12C7H4'
SOCIAL_AUTH_VK_OAUTH2_SCOPE = ['email',]

# https://console.developers.google.com/project
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '120187289110-cg5fcp2a1cifad7jeqv4bouff63mdbs8.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'aw5JIIGE7uEjW5Yxp2IL5Tyg'
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = ['email',]


SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.user.get_username',
    'social.pipeline.social_auth.associate_by_email',
    'social.pipeline.user.create_user',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'users.pipeline.user_details',
    'users.pipeline.save_avatar',
)


CRISPY_TEMPLATE_PACK = 'bootstrap3'


IGNORABLE_404_URLS = (
    re.compile(r'^/apple-touch-icon.*\.png$'),
    re.compile(r'^/favicon\.ico$'),
    re.compile(r'^/robots\.txt$'),
)


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(name)s %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console':{
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
        },
        'logfile': {
            'level': 'INFO',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': os.path.join(BASE_DIR, 'log', 'django.log'),
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['logfile', 'console'],
            'level': 'ERROR',
            'propagate': False,
        },
        'django': {
            'handlers': ['logfile', 'console'],
            'level': 'INFO',
            'propagate': False,
        },
        # 'py.warnings': {
        #     'handlers': ['null', ],
        # },
        '': {
            'handlers': ['console',],
            'level': "INFO",
            'propagate': False,
        },
    }
}
