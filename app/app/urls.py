# -*- coding: utf-8 -*-

"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.views.generic.base import RedirectView, TemplateView
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from pages.views import HomeView, PageView, TeaserView



urlpatterns = [
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'', include('social.apps.django_app.urls', namespace='social')),
    url(r'^robots\.txt$', TemplateView.as_view(template_name='base/robots.txt', content_type='text/plain')),
    url(r'^favicon\.ico$', RedirectView.as_view(url=settings.STATIC_URL + 'img/favicon.ico', permanent=True)),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
]

urlpatterns += i18n_patterns(
    url(r'^admin/', admin.site.urls),
    url(r'^user/', include('users.urls')),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^talk/', include('talk.urls')),
    url(r'^quiz/', include('quiz.urls')),
    url(r'^report/', include('report.urls')),
    url(r'^$', TeaserView.as_view(), name='teaser'),
    url(r'^home/$', HomeView.as_view(), name='home'),
    # всегда последней строчкой
    url(r'^(?P<slug>[-_\w]+)/$', PageView.as_view(), name='page'),
)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += staticfiles_urlpatterns()


urlpatterns += i18n_patterns(
    # всегда идет последней!!! проверка оставшихся урлов в базе
    url(r'^(?P<slug>[-_\w]+)/$', PageView.as_view(), name='page'),
)
