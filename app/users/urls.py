from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required, permission_required

from users.views import UserView, AvatarUpdateView, UserInfoUpdateView, UserNameUpdateView


urlpatterns = [
    url(r'^profile/$', login_required(UserView.as_view()), name='profile'),
    url(r'^(?P<pk>\d+)/profile/$', UserView.as_view(), name='profile'),
    url(r'^avatar/edit/$', login_required(AvatarUpdateView.as_view()), name='avatar_edit'),
    url(r'^info/edit/$', login_required(UserInfoUpdateView.as_view()), name='user_info_edit'),
    url(r'^name/edit/$', login_required(UserNameUpdateView.as_view()), name='user_name_edit'),
]
