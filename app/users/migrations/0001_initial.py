# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-15 10:43
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('avatar', sorl.thumbnail.fields.ImageField(blank=True, null=True, upload_to='avatar', verbose_name='avatar')),
                ('info', models.TextField(blank=True, help_text='\u0448\u043a\u043e\u043b\u0430, \u0432\u0443\u0437 \u0438 \u0442.\u0434.', null=True, verbose_name='info')),
                ('user', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'profile',
                'verbose_name_plural': 'profiles',
            },
        ),
    ]
