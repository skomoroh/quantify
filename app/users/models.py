# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _, get_language
from django.db import models
from django.contrib.auth.models import User
from sorl.thumbnail import ImageField
# TODO добавить справочник с городами
# from **.models import City


class Profile(models.Model):
    user = models.OneToOneField(User, blank=True, null=True)
    avatar = ImageField(_('avatar'), upload_to='avatar', blank=True, null=True)
    # city = models.ForeignKey(City, verbose_name=_('city'), blank=True, null=True)
    info = models.TextField(_('info'), blank=True, null=True, help_text='школа, вуз и т.д.')

    class Meta:
        verbose_name = _('profile')
        verbose_name_plural = _('profiles')

    def __unicode__(self):
        return u'%s' % self.user.username


def get_user_name(self):
    if self.first_name:
        name = '%s %s' % (self.first_name, self.last_name)
    else:
        name = '%s' % self.username
    return name

setattr(User, 'get_user_name', property(get_user_name))
