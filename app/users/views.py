# -*- coding: utf-8 -*-

from django.views.generic import View, TemplateView, DetailView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect
from django.http import Http404
from django.contrib.auth.models import User
from users.models import Profile


class UserOwnerMixin(object):
    def get_object(self, *args, **kwargs):
        obj = super(UserOwnerMixin, self).get_object(*args, **kwargs)
        if obj.user != self.request.user:
            raise Http404
        return obj


class UserView(DetailView):
    model = User
    template_name = 'users/profile.html'

    def get_object(self):
        pk = self.kwargs.get('pk', None)
        if pk:
            user = get_object_or_404(User, pk=pk)
        else:
            user = self.request.user
        return user

    def get_context_data(self, **kwargs):
        context = super(UserView, self).get_context_data(**kwargs)
        user = context['user']
        context['profile'], crt = Profile.objects.get_or_create(user=user)
        return context


class ProfileUpdateMixin(UpdateView):
    model = Profile
    fields = ['avatar', 'info']
    template_name = 'form_default.html'
    success_url = reverse_lazy('profile')

    def get_object(self):
        profile, crt = Profile.objects.get_or_create(user=self.request.user)
        return profile


class AvatarUpdateView(ProfileUpdateMixin):
    fields = ['avatar',]
    template_name = 'users/avatar_form.html'


class UserInfoUpdateView(ProfileUpdateMixin):
    fields = ['info',]


class UserNameUpdateView(UpdateView):
    model = User
    fields = ['first_name', 'last_name']
    template_name = 'form_default.html'
    success_url = reverse_lazy('profile')

    def get_object(self):
        return self.request.user
