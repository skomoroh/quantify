# -*- coding: utf-8 -*-

from django.contrib import admin
# from registration.models import RegistrationProfile
from sorl.thumbnail.admin import AdminImageMixin
from users.models import Profile

# admin.site.unregister(RegistrationProfile)

class ProfileAdmin(AdminImageMixin, admin.ModelAdmin):
    list_display = ('user',)
    raw_id_fields = ('user',)
    search_fields = ('user__id', 'user__email',)
    ordering = ('-user_id', )

admin.site.register(Profile, ProfileAdmin)
