# -*- coding: utf-8 -*-

import os
from urlparse import urlparse
from requests import request, HTTPError
from django.core.files.base import ContentFile
from users.models import Profile

def user_details(strategy, details, user=None, *args, **kwargs):
    """Update user details using data from provider."""
    if user:
        changed = False  # flag to track changes
        protected = ('username', 'id', 'pk', 'email') + \
            tuple(strategy.setting('PROTECTED_USER_FIELDS', []))

        # Update user model attributes with the new data sent by the current
        # provider. Update on some attributes is disabled by default, for
        # example username and id fields. It's also possible to disable update
        # on fields defined in SOCIAL_AUTH_PROTECTED_FIELDS.
        for name, value in details.items():
            if value and hasattr(user, name):
                current_value = getattr(user, name, None)
                if not current_value or name not in protected:
                    changed |= current_value != value
                    setattr(user, name, value)

        if changed:
            strategy.storage.user.changed(user)


def save_avatar(backend, user, response, details, is_new=False, *args, **kwargs):
    profile, crt = Profile.objects.get_or_create(user=user)
    if crt:
        img_url = None
        if backend.name == 'vk-oauth2':
            img_url = response.get('user_photo', None)
        elif backend.name == 'facebook':
            img_url = 'http://graph.facebook.com/%s/picture?type=large' % response['id']
        elif backend.name == 'google-oauth2':
            img_url = response.get('image', {}).get('url')

        if img_url:
            try:
                response = request('GET', img_url)
                response.raise_for_status()
            except HTTPError:
                pass
            else:
                profile.avatar.save('%s_%s' % (backend.name, os.path.basename(urlparse(img_url).path)), ContentFile(response.content))
                profile.save()

