# -*- coding: utf-8 -*-

from django import forms
from modeltranslation.forms import TranslationModelForm
from django.utils.translation import ugettext_lazy as _, get_language
from quiz.models import Question, Answer


class QuizDumpForm(forms.Form):
    questions = forms.ModelMultipleChoiceField(
            label=_('questions'),
            queryset=Question.objects.order_by('-id').all(),
            # widget=forms.CheckboxSelectMultiple,
            help_text=_('many - CTRL+click'),
        )

class QuizSliceForm(forms.Form):
    rows = forms.ModelChoiceField(
            label=_('rows'),
            queryset=Question.objects.order_by('-id').all(),
        )
    columns = forms.ModelMultipleChoiceField(
            label=_('columns'),
            queryset=Question.objects.order_by('-id').all(),
            # widget=forms.CheckboxSelectMultiple,
            help_text=_('many - CTRL+click'),
        )
