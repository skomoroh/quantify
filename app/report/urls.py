# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse, reverse_lazy

from report.views import QuizDumpView, QuizSliceView


urlpatterns = [
    url(r'^dump/$', login_required(QuizDumpView.as_view()), name='report_dump'),
    url(r'^slice/$', login_required(QuizSliceView.as_view()), name='report_slice'),
]
