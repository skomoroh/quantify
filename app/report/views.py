# -*- coding: utf-8 -*-

import csv
from collections import defaultdict
from django.http import HttpResponse
from django.utils.text import slugify
from django.views.generic import View, TemplateView, DetailView, ListView
from django.views.generic.edit import FormView, FormMixin
from django.views.generic.detail import SingleObjectMixin
from django.utils.translation import ugettext_lazy as _, get_language
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import Q, Count
from django.shortcuts import get_object_or_404, redirect
from sqlalchemy.orm import aliased
from sqlalchemy import func, desc, and_, case, not_, or_
from sqlalchemy.sql.operators import le, ge
from django.contrib.auth.models import User
from quiz.models import Question, Answer, Choice, Tag
from report.forms import QuizDumpForm, QuizSliceForm
from quiz.models_sa import SessionSA, ChoiceSA


class CSVResponseMixin(object):
    '''
        Вывод csv файла для скачивания
    '''
    def render_to_response_csv(self, table, filename='file.csv', *args, **kwargs):
        '''
            table - двумерный массив
            filename - имя файла
        '''
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s"' % filename

        writer = csv.writer(response)
        for row in table:
            writer.writerow(row)
        return response


class PostTemplateResponseMixin(object):
    '''
        Вывод шаблона с результатами формы по post запросу
    '''
    post_template_name = None

    def form_valid(self, form, *args, **kwargs):
        return self.render_to_response_post(self.get_context_data())

    def render_to_response_post(self, context, **response_kwargs):
        response_kwargs.setdefault('content_type', self.content_type)
        return self.response_class(
            request=self.request,
            template=self.post_template_name,
            context=context,
            using=self.template_engine,
            **response_kwargs
        )



class QuizDumpView(CSVResponseMixin, FormView):
    form_class = QuizDumpForm
    template_name = 'reports/dump_form.html'

    def form_valid(self, form, *args, **kwargs):
        questions = form.cleaned_data['questions']
        table = []
        data = defaultdict(dict)
        data_q = defaultdict(dict)
        fields = []
        q_row = [None,]
        a_row = ['user',]
        a_q = {}
        for a in Answer.objects.select_related('question').filter(question__in=questions).order_by('question', 'id'):
            fields.append(a.id)
            q_row.append(a.question.question.encode('utf-8'))
            a_row.append(a.answer.encode('utf-8'))
            a_q[a.id] = a.question_id
        table.append(q_row)
        table.append(a_row)
        for c in Choice.objects.filter(question__in=questions):
            data[c.user_id][c.answer_id] = 1
            data_q[c.user_id][c.question_id] = 0
        for u_id in data.keys():
            row = ['*',]
            for a_id in fields:
                show = data[u_id].get(a_id, None)
                if show is None:
                    show = data_q[u_id].get(a_q[a_id], None)
                row.append(show)
            table.append(row)
        # print questions
        return self.render_to_response_csv(table, 'dump.csv')


class QuizSliceView(PostTemplateResponseMixin, FormView):
    form_class = QuizSliceForm
    template_name = 'reports/slice_form.html'
    post_template_name = 'reports/slice_table.html'

    def quiz_slice(self, qy, qxs):

        ay_ids = []
        ax_ids = []

        thead = {
            1: [{'col': 2, 'row': 2, 'text': qy.question},],
            2: [],
        }
        for q in qxs:
            al =  q.answer.all()
            thead[1].append({'col': len(al), 'row': 1, 'text': q.question})
            for a in al:
                thead[2].append({'col': 1, 'row': 1, 'text': a.answer})
                ax_ids.append(a.id)

        y_title = {}
        for ay in qy.answer.all():
            ay_ids.append(ay.id)
            y_title[ay.id] = ay.answer

        base = dict()
        data = defaultdict(dict)
        total = dict()
        total_full = dict()

        session = SessionSA()

        query_y = session.query(
            ChoiceSA.answer_id.label('answer_id'),
            ChoiceSA.user_id.label('user_id'),
        ).filter(
            ChoiceSA.answer_id.in_(ay_ids)
        ).distinct()
        tab_y = query_y.subquery()

        query_x = session.query(
            ChoiceSA.answer_id.label('answer_id'),
            ChoiceSA.user_id.label('user_id'),
        ).filter(
            ChoiceSA.answer_id.in_(ax_ids)
        ).distinct()
        tab_x = query_x.subquery()

        query = session.query(
            tab_y.c.answer_id.label('ay_id'),
            tab_x.c.answer_id.label('ax_id'),
            func.count(tab_y.c.user_id).label('count'),
        ).join(
            tab_x, tab_y.c.user_id==tab_x.c.user_id
        ).group_by(
            tab_y.c.answer_id,
            tab_x.c.answer_id,
        )
        for r in query.all():
            data[r.ay_id][r.ax_id] = r.count

        query = session.query(
            tab_y.c.answer_id.label('ay_id'),
            func.count(tab_y.c.user_id).label('count'),
        ).group_by(
            tab_y.c.answer_id,
        )
        for r in query.all():
            base[r.ay_id] = r.count

        query = session.query(
            ChoiceSA.user_id.label('user_id'),
        ).filter(
            ChoiceSA.answer_id.in_(ay_ids)
        ).distinct()
        base_total = query.count()

        query = session.query(
            tab_x.c.answer_id.label('ax_id'),
            func.count(tab_x.c.user_id).label('count'),
        ).group_by(
            tab_x.c.answer_id,
        )
        for r in query.all():
            total_full[r.ax_id] = r.count

        tsq = session.query(
            tab_x.c.answer_id.label('ax_id'),
            tab_x.c.user_id.label('user_id'),
        ).join(
            tab_y, tab_y.c.user_id==tab_x.c.user_id
        ).distinct(
        ).subquery()
        query = session.query(
            tsq.c.ax_id.label('ax_id'),
            func.count(tsq.c.user_id).label('count'),
        ).group_by(
            tsq.c.ax_id
        )
        for r in query.all():
            total[r.ax_id] = r.count

        session.close()

        table = []
        for ay_id in ay_ids:
            row = [y_title[ay_id], base.get(ay_id, 0)]
            for ax_id in ax_ids:
                row.append(data.get(ay_id, {}).get(ax_id, 0))
            table.append(row)

        tsum = [_('users'), base_total]
        for ax_id in ax_ids:
            tsum.append(
                '%s (%s)' % (total.get(ax_id, 0), total_full.get(ax_id, 0)),
            )

        return {
            'thead': thead,
            'table': table,
            'tsum': tsum,
        }

    def form_valid(self, form, *args, **kwargs):
        context_data = self.get_context_data()

        qy = form.cleaned_data['rows']
        qxs = form.cleaned_data['columns']
        context_data.update(self.quiz_slice(qy, qxs))

        return self.render_to_response_post(context_data)
        # return self.render_to_response(context_data)
