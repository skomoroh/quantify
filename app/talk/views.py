# -*- coding: utf-8 -*-

from django.views.generic import View, TemplateView, DetailView, ListView
from django.views.generic.edit import CreateView
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect
from django.http import Http404
from django.contrib.auth.models import User
from talk.models import Message


def message_new_count(user):
    return Message.objects.filter(receiver=user, is_open=False).count()


class MessageListView(ListView):
    model = Message
    template_name = 'talk/message_list.html'
    paginate_by = 20

    def get_queryset(self):
        queryset = super(MessageListView, self).get_queryset()
        folder = self.kwargs.get('folder', None)
        if folder == 'new':
            queryset = queryset.filter(receiver=self.request.user, is_open=False)
        elif folder == 'inbox':
            queryset = queryset.filter(receiver=self.request.user)
        elif folder == 'sent':
            queryset = queryset.filter(sender=self.request.user)
        else:
            raise Http404
        queryset = queryset.select_related('sender', 'receiver',)
        queryset = queryset.order_by('-created')
        return queryset


class MessageView(DetailView):
    model = Message
    template_name = 'talk/message.html'

    def get_object(self):
        message = super(MessageView, self).get_object()
        if message.sender != self.request.user and message.receiver != self.request.user:
            raise Http404
        if message.receiver == self.request.user:
            message.is_open = True
            message.save()
        return message


class MessageCreateView(CreateView):
    model = Message
    fields = ['title', 'text']
    template_name = 'form_default.html'
    success_url = reverse_lazy('message_list_sent')

    def form_valid(self, form):
        user_id = self.kwargs.get('user_id', None)
        user = get_object_or_404(User, pk=user_id)
        form.instance.receiver = user
        form.instance.sender = self.request.user
        return super(MessageCreateView, self).form_valid(form)

