# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _, get_language
from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse


class Message(models.Model):
    sender = models.ForeignKey(User, verbose_name=_('sender'), related_name='message_from')
    receiver = models.ForeignKey(User, verbose_name=_('receiver'), related_name='message_to')
    title = models.CharField(_('title'), max_length=255,)
    text = models.TextField(_('text'))
    is_open = models.BooleanField(_('is open'), default=False, db_index=True,)
    created = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        verbose_name = _('message')
        verbose_name_plural = _('messages')

    def __unicode__(self):
        return u'%s' % self.id
