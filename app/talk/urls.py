# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse, reverse_lazy

from talk.views import MessageListView, MessageView, MessageCreateView


urlpatterns = [
    url(r'^(?P<pk>\d+)/$', login_required(MessageView.as_view()), name='message_view'),
    url(r'^user/(?P<user_id>\d+)/$', login_required(MessageCreateView.as_view()), name='message_send'),

    url(r'^list/new/$', login_required(MessageListView.as_view()), {'folder': 'new'}, name='message_list_new'),
    url(r'^list/inbox/$', login_required(MessageListView.as_view()), {'folder': 'inbox'}, name='message_list_inbox'),
    url(r'^list/sent/$', login_required(MessageListView.as_view()), {'folder': 'sent'}, name='message_list_sent'),
]
