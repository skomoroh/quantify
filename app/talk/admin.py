# -*- coding: utf-8 -*-

from django.contrib import admin
from talk.models import Message


class MessageAdmin(admin.ModelAdmin):
    list_display = ('sender', 'receiver', 'is_open', 'created')
    raw_id_fields = ('sender', 'receiver',)
    list_filter = ('is_open', 'created')
    search_fields = ('sender__id', 'sender__email', 'receiver__id', 'receiver__email')
    ordering = ('-created', )

admin.site.register(Message, MessageAdmin)
