# -*- coding: utf-8 -*-

from talk.views import message_new_count

def talk(request):
    if request.user.is_authenticated():
        return {'message_new_count': message_new_count(request.user)}
    else:
        return {}
