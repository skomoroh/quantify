# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _, get_language
from django.db import models
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField


class Page(models.Model):
    title = models.CharField(_('title'), max_length=255,)
    slug = models.SlugField('url', max_length=255, unique=True)
    text = RichTextUploadingField(_('text'), blank=True, null=True)

    class Meta:
        verbose_name = _('page')
        verbose_name_plural = _('pages')

    def __unicode__(self):
        return u'%s' % (self.slug)
