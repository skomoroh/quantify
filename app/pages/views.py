# -*- coding: utf-8 -*-

from django.views.generic import View, TemplateView, DetailView, ListView
from django.utils.translation import ugettext_lazy as _, get_language
from django.core.urlresolvers import reverse, reverse_lazy
from pages.models import Page


class PageView(DetailView):
    model = Page
    template_name = 'pages/page.html'


class HomeView(TemplateView):
    template_name = 'pages/home.html'

    def get_context_data(self, *args, **kwargs):
        context = super(HomeView, self).get_context_data(*args, **kwargs)
        page = Page.objects.get(slug='home')
        context['title'] = page.title
        context['text'] = page.text
        return context


class TeaserView(TemplateView):
    template_name = 'pages/teaser_en.html'


