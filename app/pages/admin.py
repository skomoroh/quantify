# -*- coding: utf-8 -*-

from django.contrib import admin
from modeltranslation.admin import TranslationAdmin, TabbedTranslationAdmin, TranslationTabularInline
from pages.models import Page


class PageAdmin(TabbedTranslationAdmin):
    list_display = ('slug', 'title',)
    # list_filter = ('in_menu',)
    search_fields = ('title',)
    ordering = ('-id', )
    # prepopulated_fields = {"slug": ("title",)}
    # list_editable = ('in_menu',)
    # readonly_fields = ('slug',)

admin.site.register(Page, PageAdmin)
