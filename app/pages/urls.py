# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse, reverse_lazy

from pages.views import PageView


urlpatterns = [
    url(r'^(?P<slug>[-_\w]+)/$', PageView.as_view(), name='page'),
]
