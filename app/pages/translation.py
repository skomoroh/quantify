# -*- coding: utf-8 -*-

from modeltranslation.translator import translator, TranslationOptions
from pages.models import Page


class PageTranslationOptions(TranslationOptions):
    fields = ('title', 'text',)

translator.register(Page, PageTranslationOptions)

