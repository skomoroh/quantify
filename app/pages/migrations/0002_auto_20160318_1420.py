# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-18 11:20
from __future__ import unicode_literals

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='text',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='text'),
        ),
        migrations.AlterField(
            model_name='page',
            name='text_en',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='text'),
        ),
        migrations.AlterField(
            model_name='page',
            name='text_ru',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='text'),
        ),
    ]
