# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _, get_language
from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.conf import settings


class Tag(models.Model):
    tag = models.CharField(_('tag'), max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = _('tag')
        verbose_name_plural = _('tags')

    def __unicode__(self):
        lang = get_language()
        return u'%s' % getattr(self, 'tag_%s' % lang.lower(), None)


class Question(models.Model):
    NONE = 0
    MODERATE = 2
    SHOW = 1

    SELECT_ONE = 1
    SELECT_MULTI = 2

    action = models.IntegerField(_('action'),
        choices=(
            (SELECT_ONE, _('select one')),
            (SELECT_MULTI, _('select multi')),
        ),
        default=SELECT_ONE, db_index=True,
    )
    status = models.IntegerField(_('status'),
        choices=(
            (NONE, _('none')),
            (MODERATE, _('to moderate')),
            (SHOW, _('show')),
        ),
        default=NONE, db_index=True,
    )
    user = models.ForeignKey(User, verbose_name=_('author'))
    question = models.TextField(_('question'), blank=True, null=True)

    tag = models.ManyToManyField(Tag, verbose_name=_('tag'), related_name='question')
    created = models.DateTimeField(auto_now_add=True, db_index=True)
    # modified = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        verbose_name = _('question')
        verbose_name_plural = _('questions')

    def __unicode__(self):
        lang = get_language()
        return u'%s' % getattr(self, 'question_%s' % lang.lower(), None)

    def get_absolute_url(self):
        return reverse('question', kwargs={'pk': self.pk})

    @property
    def is_show(self):
        lang = get_language()
        return getattr(self, 'status_%s' % lang.lower(), None) == self.SHOW

    @property
    def is_edit(self):
        lang = get_language()
        return getattr(self, 'status_%s' % lang.lower(), None) not in (self.SHOW, self.NONE)

    @property
    def is_none(self):
        lang = get_language()
        return getattr(self, 'status_%s' % lang.lower(), None) == self.NONE

    def get_status_display(self):
        lang = get_language()
        return getattr(self, 'get_status_%s_display' % lang.lower())()

    def lang_show(self):
        data = []
        for k, v in settings.LANGUAGES:
            if getattr(self, 'status_%s' % k.lower(), None) == self.SHOW:
                data.append(k)
        return data

    def lang_not_none(self):
        data = []
        for k, v in settings.LANGUAGES:
            if getattr(self, 'status_%s' % k.lower(), None) in (self.SHOW, self.MODERATE):
                data.append(k)
        return data

    def user_answer(self, user):
        # TODO сделать проверку: отвечал или нет на вопрос
        return False


class Answer(models.Model):
    question = models.ForeignKey(Question, verbose_name=_('question'), related_name='answer')
    answer = models.CharField(_('answer'), max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = _('answer')
        verbose_name_plural = _('answers')
        ordering = ['id']

    def __unicode__(self):
        lang = get_language()
        return u'%s' % getattr(self, 'answer_%s' % lang.lower(), None)



class Choice(models.Model):
    user = models.ForeignKey(User, verbose_name=_('author'))
    question = models.ForeignKey(Question, verbose_name=_('question'), related_name='choice')
    answer = models.ForeignKey(Answer, verbose_name=_('answer'), related_name='choice')

    created = models.DateTimeField(auto_now_add=True, db_index=True)
    useragent = models.CharField(_('useragent'), max_length=1023, blank=True, null=True)

    class Meta:
        verbose_name = _('choice')
        verbose_name_plural = _('choices')

    def __unicode__(self):
        return u'%s' % self.id



