# -*- coding: utf-8 -*-

from django.contrib import admin
from modeltranslation.admin import TranslationAdmin, TabbedTranslationAdmin, TranslationTabularInline
from quiz.models import Question, Answer, Choice, Tag


class TagAdmin(TabbedTranslationAdmin):
    # readonly_fields=('id',)
    list_display = ('tag',)
    search_fields = ('tag',)
    ordering = ('tag',)

admin.site.register(Tag, TagAdmin)


class TagInline(TranslationTabularInline):
    model = Tag
    extra = 1


class AnswerInline(TranslationTabularInline):
    model = Answer
    extra = 1


class QuestionAdmin(TabbedTranslationAdmin):
    list_display = ('id', 'status', 'user', 'question', 'created')
    list_filter = ('status', 'created', 'tag')
    search_fields = ('question',)
    ordering = ('-created',)
    raw_id_fields = ('user',)
    filter_horizontal = ('tag',)
    # readonly_fields = ()
    inlines = [
        AnswerInline,
        # TagInline,
    ]

admin.site.register(Question, QuestionAdmin)


class ChoiceAdmin(admin.ModelAdmin):
    # readonly_fields=('id',)
    list_display = ('id', 'user', 'question', 'answer', 'created',)
    search_fields = ('question__question',)
    list_filter = ('created',)
    ordering = ('created',)

admin.site.register(Choice, ChoiceAdmin)
