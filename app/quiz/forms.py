# -*- coding: utf-8 -*-

from django import forms
from modeltranslation.forms import TranslationModelForm
from django.utils.translation import ugettext_lazy as _, get_language
from quiz.models import Question, Answer


class QuestionForm(TranslationModelForm):
    class Meta:
        model = Question
        fields = ['question', 'action',]


class AnswerForm(TranslationModelForm):
    class Meta:
        model = Answer
        fields = ['answer',]


class QuizForm(forms.Form):
    def __init__(self, action, queryset, *args, **kwargs):
        super(QuizForm, self).__init__(*args, **kwargs)
        if action == Question.SELECT_MULTI:
            self.fields['choices'] = forms.ModelMultipleChoiceField(label=_('choices'), queryset=queryset, widget=forms.CheckboxSelectMultiple)
        elif action == Question.SELECT_ONE:
            self.fields['choices'] = forms.ModelChoiceField(label=_('choices'), queryset=queryset, empty_label=None, widget=forms.RadioSelect)
        # if action == Question.SELECT_MULTI:
        #     self.fields['choices'] = forms.MultipleChoiceField(label=_('choices'), choices=choices, widget=forms.CheckboxSelectMultiple)
        # elif action == Question.SELECT_ONE:
        #     self.fields['choices'] = forms.ChoiceField(label=_('choices'), choices=choices, widget=forms.RadioSelect)
