# -*- coding: utf-8 -*-

from django.views.generic import View, TemplateView, DetailView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView, FormMixin
from django.views.generic.detail import SingleObjectMixin
from extra_views import CreateWithInlinesView, UpdateWithInlinesView, InlineFormSet
from django.utils.translation import ugettext_lazy as _, get_language
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import Q, Count
from django.db import transaction
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.models import User
import json
from quiz.models import Question, Answer, Choice, Tag
from quiz.forms import QuestionForm, AnswerForm, QuizForm
from users.views import UserOwnerMixin


class QuestionView(DetailView):
    model = Question
    template_name = 'quiz/question.html'

    def get_queryset(self):
        queryset = super(QuestionView, self).get_queryset()
        # queryset = queryset.prefetch_related('answer',)
        return queryset


class QuizFormView(SingleObjectMixin, FormView):
    model = Question
    form_class = QuizForm
    template_name = 'quiz/quiz.html'

    def dispatch(self, request, *args, **kwargs):
        # исправление ошибки в джангу, теряет указатель на обект при вызове миксина
        self.object = self.get_object()
        return super(QuizFormView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        next = self.kwargs.get('next', None)
        if next:
            return next
        else:
            return reverse('quiz_choice', kwargs={'pk': self.object.pk})

    def get_form_kwargs(self, *args, **kwargs):
        question = self.object
        kwargs = super(QuizFormView, self).get_form_kwargs(*args, **kwargs)
        kwargs['queryset'] = question.answer.all()
        kwargs['action'] = question.action
        return kwargs

    def form_valid(self, form, *args, **kwargs):
        question = self.object
        user = self.request.user
        useragent = self.request.META.get('HTTP_USER_AGENT', None)
        data = form.cleaned_data
        if self.object.action == Question.SELECT_MULTI:
            answers = data['choices']
        elif self.object.action == Question.SELECT_ONE:
            answers = [data['choices'],]
        else:
            answers = []
        with transaction.atomic():
            Choice.objects.filter(user=user, question=question).delete()
            for answer in answers:
                Choice.objects.create(user=user, question=question, answer=answer, useragent=useragent)
        return super(QuizFormView, self).form_valid(form, *args, **kwargs)



class QuizStatView(DetailView):
    model = Question
    template_name = 'quiz/quiz_stat.html'

    def get_queryset(self):
        queryset = super(QuizStatView, self).get_queryset()
        # queryset = queryset.prefetch_related('answer',)
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super(QuizStatView, self).get_context_data(*args, **kwargs)
        question = self.get_object()
        queryset = question.choice.all()
        # user_count = User.objects.filter(choice__question=question).annotate(count=Count('pk', distinct=True))

        user_count_rows = queryset.values('question').annotate(count=Count('user', distinct=True))
        if user_count_rows:
            user_count = user_count_rows[0].get('count')
            answer_count = dict([(row['answer'], row['count']) for row in queryset.values('answer').annotate(count=Count('user', distinct=True))])
        else:
            user_count = 0
            answer_count = {}
        context['user_count'] = user_count
        table = []
        for i, row in enumerate(question.answer.order_by('id').all()):
            table.append((i+1, row.answer, answer_count.get(row.pk, 0), 100.0*answer_count.get(row.pk, 0)/user_count if user_count else 0))
        context['table'] = table
        context['chart'] = {
            'categories': json.dumps([row[1] for row in table], ensure_ascii=False),
            'data': json.dumps([row[2] for row in table], ensure_ascii=False),
        }
        if question.action == Question.SELECT_ONE:
            context['pie'] = {
                'data': json.dumps([{'name':row[1], 'y':row[2]} for row in table if row[2]>0], ensure_ascii=False),
            }
        return context




class QuestionListView(ListView):
    model = Question
    template_name = 'quiz/question_list.html'
    paginate_by = 20

    def dispatch(self, request, *args, **kwargs):
        self.filter = self.get_filter()
        return super(QuestionListView, self).dispatch(request, *args, **kwargs)

    def get_filter(self):
        tag_ids = set(self.request.session.get('filter_tag_ids', []))
        if 'tag' in self.request.GET:
            tag_ids.add(self.request.GET['tag'])
        if 'tag_remove' in self.request.GET:
            tag_ids.remove(self.request.GET['tag_remove'])
        if 'filter' in self.request.GET and self.request.GET['filter'] == 'clear':
            tag_ids = set()
        self.request.session['filter_tag_ids'] = list(tag_ids)
        if tag_ids:
            tag = Tag.objects.filter(id__in=tag_ids)
        else:
            tag = []
        return {'tag': tag}

    def get_queryset(self):
        user_id = self.kwargs.get('user_id', None)
        show = self.kwargs.get('show_only', None)
        moderate = self.kwargs.get('moderate_only', None)
        lang = get_language()
        status_field = 'status_%s' % lang.lower()

        queryset = super(QuestionListView, self).get_queryset()
        queryset = queryset.prefetch_related('tag',)
        # queryset = queryset.select_related('tag',)
        for tag in self.filter.get('tag', []):
            queryset = queryset.filter(tag=tag)
        if user_id:
            user = get_object_or_404(User, pk=user_id)
            queryset = queryset.filter(user=user)
        if show:
            queryset = queryset.filter(**{status_field: Question.SHOW})
        if moderate:
            queryset = queryset.filter(**{status_field: Question.MODERATE})
        queryset = queryset.distinct().order_by('-created')

        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super(QuestionListView, self).get_context_data(*args, **kwargs)
        queryset = self.get_queryset()
        tag = self.filter.get('tag', [])
        context['tag_selected'] = tag
        if tag:
            context['tag_other'] = Tag.objects.filter(question__in=queryset).exclude(id__in=[t.id for t in tag]).distinct().all()
        else:
            context['tag_other'] = Tag.objects.all()
        return context


class AnswerInline(InlineFormSet):
    model = Answer
    form_class = AnswerForm
    extra = 10
    # max_num = 10


class QuestionViewMixin(object):
    model = Question
    form_class = QuestionForm
    # fields = ['question',]
    template_name = 'form_inlines.html'
    inlines = [AnswerInline,]

    def forms_valid(self, form, inlines, *args, **kwargs):
        form.instance.user = self.request.user
        form.instance.status = Question.MODERATE
        self.object = form.save()
        for formset in inlines:
            formset.save()
        return redirect(self.get_success_url())


class QuestionCreateView(QuestionViewMixin, CreateWithInlinesView):
    pass


class QuestionUpdateView(UserOwnerMixin, QuestionViewMixin, UpdateWithInlinesView):
    pass


class QuestionDeleteView(UserOwnerMixin, DeleteView):
    template_name = 'form_confirm.html'
    model = Question
    success_url = reverse_lazy('question_list')

