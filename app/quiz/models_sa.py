# -*- coding: utf-8 -*-

from django.conf import settings
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine(settings.SQLAL_DB, echo=False)
SessionSA = sessionmaker(bind=engine)

Base = declarative_base()
Base.metadata.reflect(engine)

class ChoiceSA(Base):
    __table__ = Base.metadata.tables['quiz_choice']

