# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse, reverse_lazy

from quiz.views import (
        QuestionView, QuestionListView,
        QuestionCreateView, QuestionUpdateView, QuestionDeleteView,
        QuizFormView, QuizStatView,
    )


urlpatterns = [
    url(r'^(?P<pk>\d+)/$', QuestionView.as_view(), name='question'),
    url(r'^(?P<pk>\d+)/choice/$', login_required(QuizFormView.as_view()), {'next': reverse_lazy('question_list_show')}, name='quiz_choice'),
    url(r'^(?P<pk>\d+)/stat/$', QuizStatView.as_view(), name='quiz_stat'),

    url(r'^list/$', QuestionListView.as_view(), {'show_only': True}, name='question_list_show'),
    url(r'^list/user/(?P<user_id>\d+)/$', QuestionListView.as_view(), name='question_list_user'),
    url(r'^list/moderate/$', QuestionListView.as_view(), {'moderate_only': True}, name='question_list_moderate'),

    url(r'^add/$', login_required(QuestionCreateView.as_view()), name='question_add'),
    url(r'^(?P<pk>\d+)/edit/$', login_required(QuestionUpdateView.as_view()), name='question_edit'),
    url(r'^(?P<pk>\d+)/del/$', login_required(QuestionDeleteView.as_view()), name='question_del'),
]
