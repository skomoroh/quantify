# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-09 17:37
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0003_auto_20160309_1731'),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('answer', models.TextField(verbose_name='answer')),
                ('answer_ru', models.TextField(null=True, verbose_name='answer')),
                ('answer_en', models.TextField(null=True, verbose_name='answer')),
            ],
            options={
                'verbose_name': 'answer',
                'verbose_name_plural': 'answers',
            },
        ),
        migrations.RemoveField(
            model_name='question',
            name='modified',
        ),
        migrations.AlterField(
            model_name='question',
            name='question',
            field=models.TextField(verbose_name='question'),
        ),
        migrations.AlterField(
            model_name='question',
            name='question_en',
            field=models.TextField(null=True, verbose_name='question'),
        ),
        migrations.AlterField(
            model_name='question',
            name='question_ru',
            field=models.TextField(null=True, verbose_name='question'),
        ),
        migrations.AlterField(
            model_name='question',
            name='status',
            field=models.IntegerField(choices=[(0, 'none'), (2, 'to moderate'), (1, 'show')], db_index=True, default=0, verbose_name='status'),
        ),
        migrations.AlterField(
            model_name='question',
            name='status_en',
            field=models.IntegerField(choices=[(0, 'none'), (2, 'to moderate'), (1, 'show')], db_index=True, default=0, null=True, verbose_name='status'),
        ),
        migrations.AlterField(
            model_name='question',
            name='status_ru',
            field=models.IntegerField(choices=[(0, 'none'), (2, 'to moderate'), (1, 'show')], db_index=True, default=0, null=True, verbose_name='status'),
        ),
        migrations.AddField(
            model_name='answer',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='answer', to='quiz.Question', verbose_name='question'),
        ),
    ]
