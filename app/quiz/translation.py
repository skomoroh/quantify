# -*- coding: utf-8 -*-

from modeltranslation.translator import translator, TranslationOptions
from quiz.models import Question, Answer, Tag


class QuestionTranslationOptions(TranslationOptions):
    fields = ('question', 'status',)

translator.register(Question, QuestionTranslationOptions)


class AnswerTranslationOptions(TranslationOptions):
    fields = ('answer',)

translator.register(Answer, AnswerTranslationOptions)


class TagTranslationOptions(TranslationOptions):
    fields = ('tag',)

translator.register(Tag, TagTranslationOptions)
