# -*- coding: utf-8 -*-

import logging
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.contrib.auth.models import User
import random
from collections import defaultdict
from quiz.models import Question, Answer, Choice, Tag


logger = logging.getLogger('commands')


def get_rand_one(arr, first=None):
    l = len(arr)
    x = random.randrange(1, l)
    if first is None:
        first = random.choice([True, False])
    if first:
        a, b = 0, x
    else:
        a, b = x, l
    i = random.randrange(a, b)
    return arr[i]


def get_rand_multi(arr):
    l = len(arr)
    first = random.choice([True, False])
    return set([get_rand_one(arr, first) for i in range(random.randrange(1, l))])




class Command(BaseCommand):
    help = 'set test data'

    def add_arguments(self, parser):
        parser.add_argument('--del',
            action='store_true',
            dest='delete',
            default=False,
            help='Delete before insert')

    # @transaction.atomic
    def handle(self, *args, **options):
        logger.info('START set test data')

        if options['delete']:
            User.objects.exclude(pk=1).exclude(username='root').exclude(username='admin').all().delete()

        new_user_list = []
        for i in range(1000):
            new_user_list.append(
                User(username='user%s' % i, first_name='имя', last_name=i)
            )
        User.objects.bulk_create(new_user_list)
        logger.info('create users')

        user_ids = [u.id for u in User.objects.all()]

        if options['delete']:
            Question.objects.all().delete()

        new_qustion_list = []
        for i in range(500):
            new_qustion_list.append(
                Question(
                    action=Question.SELECT_ONE, user_id=random.choice(user_ids),
                    status_ru=Question.SHOW, status_en=Question.SHOW,
                    question_ru='Вопрос %s (селект)' % i, question_en='Question %s (select)' % i,
                )
            )
        for i in range(500, 1000):
            new_qustion_list.append(
                Question(
                    action=Question.SELECT_MULTI, user_id=random.choice(user_ids),
                    status_ru=Question.SHOW, status_en=Question.SHOW,
                    question_ru='Вопрос %s (мультиселект)' % i, question_en='Question %s (multiselect)' % i,
                )
            )
        Question.objects.bulk_create(new_qustion_list)
        logger.info('create questions')

        qustion_one_ids = [q.id for q in Question.objects.filter(action=Question.SELECT_ONE).all()]
        qustion_multi_ids = [q.id for q in Question.objects.filter(action=Question.SELECT_MULTI).all()]

        if options['delete']:
            Answer.objects.all().delete()

        new_answer_list = []
        for q_id in qustion_one_ids:
            for i in range(random.randint(2, 10)):
                new_answer_list.append(
                    Answer(
                        question_id=q_id,
                        answer_ru='Ответ %s (%s)' % (i, q_id), answer_en='Answer %s (%s)' % (i, q_id),
                    )
                )
        for q_id in qustion_multi_ids:
            for i in range(random.randint(5, 10)):
                new_answer_list.append(
                    Answer(
                        question_id=q_id,
                        answer_ru='Ответ %s (%s)' % (i, q_id), answer_en='Answer %s (%s)' % (i, q_id),
                    )
                )
        Answer.objects.bulk_create(new_answer_list)
        logger.info('create answers')

        answer_ids = defaultdict(list)
        for a in Answer.objects.all():
            answer_ids[a.question_id].append(a.id)

        random.shuffle(user_ids)

        if options['delete']:
            Choice.objects.all().delete()

        new_choice_data = set()
        for u_id in user_ids[:300]:
            for q_id in qustion_one_ids:
                a_id = get_rand_one(answer_ids[q_id])
                new_choice_data.add((u_id, q_id, a_id))
        logger.info('get choice one 300x500')
        for u_id in user_ids[:300]:
            for q_id in qustion_multi_ids:
                for a_id in get_rand_multi(answer_ids[q_id]):
                    new_choice_data.add((u_id, q_id, a_id))
        logger.info('get choice multi 300x500')
        for u_id in user_ids[300:600]:
            for q_id in random.sample(qustion_one_ids, 50):
                a_id = get_rand_one(answer_ids[q_id])
                new_choice_data.add((u_id, q_id, a_id))
        logger.info('get choice one 300x50')
        for u_id in user_ids[300:600]:
            for q_id in random.sample(qustion_multi_ids, 50):
                for a_id in get_rand_multi(answer_ids[q_id]):
                    new_choice_data.add((u_id, q_id, a_id))
        logger.info('get choice multi 300x50')
        for u_id in user_ids[600:]:
            for q_id in random.sample(qustion_one_ids, 200):
                a_id = get_rand_one(answer_ids[q_id])
                new_choice_data.add((u_id, q_id, a_id))
        logger.info('get choice one 400x200')
        for u_id in user_ids[600:]:
            for q_id in random.sample(qustion_multi_ids, 200):
                for a_id in get_rand_multi(answer_ids[q_id]):
                    new_choice_data.add((u_id, q_id, a_id))
        logger.info('get choice multi 400x200')

        new_choice_list = []
        for u_id, q_id, a_id in new_choice_data:
            new_choice_list.append(Choice(user_id=u_id, question_id=q_id, answer_id=a_id))

        Choice.objects.bulk_create(new_choice_list)
        logger.info('create choice')

        logger.info('FINISH set test data')
