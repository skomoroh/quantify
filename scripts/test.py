#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import time

import logging
logging.basicConfig(
    level = logging.DEBUG,
    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s', datefmt='%Y-%m-%d %H:%M:%S',
    filename=os.path.join(os.path.dirname(os.path.abspath(__file__)), 'logs', 'test.log'),
)
logging.getLogger("django").setLevel(logging.ERROR)
logger = logging.getLogger('test')

# set django env
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.join(BASE_DIR, 'app'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.settings")
import django
django.setup()

from django.conf import settings


logger.info('start')
